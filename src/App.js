import './App.css';
import Index from "./components";
import oval from '../src/components/images/Oval.svg'

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <img style={{ position: 'absolute', left: '84px', top: '12%', zIndex: '8'}} src={oval} alt="oval"/>
          <Index />
      </header>
    </div>
  );
}

export default App;
