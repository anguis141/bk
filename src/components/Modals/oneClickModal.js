import React from 'react';
import {Box, Button, Modal, Stack, TextField, Typography} from "@mui/material";
import HighlightOffTwoToneIcon from '@mui/icons-material/HighlightOffTwoTone';
import {useStyles} from './useStyles';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 425,
    height: 620,
    bgcolor: 'radial-gradient(100% 100% at 0% 0%, rgba(148, 166, 205, 0.2) 0%, rgba(148, 166, 205, 2e-05) 100%)',
    boxShadow: 12,
    p: 4,
    backdropFilter: 'blur(27.1828px)',
    borderRadius: '20px',
    opacity: 1
};

function OneClickModal({open, handleClose}) {
const classes = useStyles();
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Stack direction="row" justifyContent="space-between">
                    <Typography id="modal-modal-title" style={{
                        color: '#FFF',
                        fontWeight: 800,
                        fontSize: '24px',
                        lineHeight: '29px'
                    }}>
                        В 1 клик
                    </Typography>
                    <HighlightOffTwoToneIcon sx={{ color: 'white', opacity: 0.5 }}/>
                </Stack>
                <Typography id="modal-modal-description" sx={{fontWeight: 800,
                    fontSize: '41px',
                    lineHeight: '46px',
                    textAlign: 'center',
                    color: '#FFFFFF',
                marginTop: '45px'}}>
                    Регистрация прошла успешно!
                </Typography>
                <Typography id="modal-modal-description" sx={{fontWeight: 400,
                    fontSize: '15px',
                    lineHeight: '18px',
                    textAlign: 'center',
                    color: '#FFFFFF',
                    marginTop: '20px'}}>
                    Не забудьте сохранить логин и пароль
                </Typography>
                <Stack direction="row" justifyContent="space-between" sx={{ marginTop: '20px' }}>
                    <TextField className={classes.textField} sx={{ bgColor: '#FFF'}} id="outlined-basic" label="Логин" variant="outlined" />
                    <TextField sx={{ bgColor: '#FFF'}} id="outlined-basic" label="Пароль" variant="outlined" />
                </Stack>
            </Box>
        </Modal>
    );
}

export default OneClickModal;