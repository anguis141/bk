import {makeStyles} from "@mui/styles";


export const useStyles = makeStyles(theme => ({
    textField:{
        '& .MuiTextField-root': {
            m: 1, width: '25ch'
        },
    },
}));