import {makeStyles} from "@mui/styles";


export const useStyles = makeStyles(theme => ({

    paymentBox: {
        position: "absolute",
        bottom: '40px',
        right: '50px'
    },
    testedBy: {
        textAlign: 'left',
        fontWeight: 700,
        fontSize: '15px',
        lineHeight: '17.9px',
        color: '#FFF'
    },
    BK: {
        fontWeight: 400,
        fontSize: '15px',
        lineHeight: '17.9px'
    }

}));