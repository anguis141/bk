import React from 'react';
import {useStyles} from './useStyles';
import {Box, Stack} from "@mui/material";
import visa from '../images/paymentsImages/visa.svg'
import qiwi from '../images/paymentsImages/qiwi.svg'
import mc from '../images/paymentsImages/mc.svg'
import payeer from '../images/paymentsImages/payeer.svg'
import webm from '../images/paymentsImages/webm.svg'
import yandex from '../images/paymentsImages/yandex.svg'
import advcash from '../images/paymentsImages/advcash.svg'
import test from '../images/paymentsImages/test.svg'

function Payments() {
    const classes = useStyles()
    return (
        <div>
            <Box className={classes.paymentBox}>
                <Box style={{display: 'flex', flexDirection: 'row', gap: '10px'}}>
                    <img src={visa} alt="visa"/>
                    <img src={mc} alt="mc"/>
                    <img src={webm} alt="webm"/>
                    <img src={yandex} alt="yandex"/>
                    <img src={advcash} alt="advcash"/>
                    <img src={qiwi} alt="qiwi"/>
                    <img src={payeer} alt="payeer"/>
                </Box>
            </Box>
            <Box style={{position: 'absolute', bottom: '40px', left: '50px'}}>
                <Stack direction="row" spacing={2}>
                    <img src={test} alt='test'/>
                    <span className={classes.testedBy}>Проверено <br/> <span className={classes.BK}> Рейтингом букмекеров</span></span>
                </Stack>
            </Box>
        </div>
    );
}

export default Payments;