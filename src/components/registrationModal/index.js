import * as React from 'react';
import {styled} from '@mui/system';
import {useStyles} from './useStyles';
import {Box, Checkbox, Divider, Stack} from "@mui/material";
import {useState} from "react";
import ReactFlagsSelect from "react-flags-select";
import ball from '../images/ball.svg'
import cubes from '../images/cubes.svg'
import tvspot from '../images/tvspot.svg'
import {
    buttonUnstyledClasses,
    TabPanelUnstyled,
    TabsListUnstyled,
    TabsUnstyled,
    TabUnstyled,
    tabUnstyledClasses
} from "@mui/base";
import OneClickModal from "../Modals/oneClickModal";

const blue = {
    50: '#F0F7FF',
    100: '#C2E0FF',
    200: '#80BFFF',
    300: '#66B2FF',
    400: '#3399FF',
    500: '#007FFF',
    600: '#0072E5',
    700: '#0059B2',
    800: '#004C99',
    900: '#003A75',
    1010: '#80C100'
};

const Tab = styled(TabUnstyled)`
  font-family: IBM Plex Sans, sans-serif;
  color: #fff;
  cursor: pointer;
  font-size: 0.875rem;
  font-weight: 600;
  background-color: transparent;
  width: 345px;
  padding: 10px 12px;
  margin: 6px 6px 6px 6px;
  border: none;
  border-radius: 7px;
  display: flex;
  justify-content: center;

  &:hover {
    background-color: ${blue[1010]};
  }

  &:focus {
    color: #fff;
  }

  &.${tabUnstyledClasses.selected} {
    background-color: #fff;
    color: #0B0E1D;
  }

  &.${buttonUnstyledClasses.disabled} {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

const TabPanel = styled(TabPanelUnstyled)(
    ({theme}) => `
  width: 345px;
  font-family: IBM Plex Sans, sans-serif;
  font-size: 0.875rem;
  padding: 10px 0;
  `,
);

const TabsList = styled(TabsListUnstyled)(
    ({theme}) => `
  max-width: 345px;
  background-color: #0000004D;
  border-radius: 12px;
  margin-bottom: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  align-content: space-between;
  `,
);

function Registration() {
    const classes = useStyles()
    const [select, setSelect] = useState("RU");
    const onSelect = (code) => setSelect(code);
    const [currentIndex, setCurrentIndex] = useState(0);
    const [openOnClick, setOpenOneClick] = useState(false);
    const [openEmail, setOpenEmail] = useState(false);
    const [openMedia, setOpenMedia] = useState(false);

    console.log(currentIndex)
    const handleOpen = (type) => {
        if (type === 'one') {
            setOpenOneClick(true);
        } else if (type === 'email') {
            setOpenEmail(true);
        } else setOpenMedia(true)
    };
    const handleClose = (type) => {
        if (type === 'one') {
            setOpenOneClick(false);
        } else if (type === 'email') {
            setOpenEmail(false);
        } else setOpenMedia(false)
    };
    return (
        <Box className={classes.root}>
            <Box className={classes.mainWrapper}>
                <Box style={{marginTop: '20px'}}>
                    <span className={classes.title}>Бонус до 50 000 всем клиентам</span>
                    <div style={{fontSize: '15px', color: '#FFF', marginTop: '10px', fontWeight: '400'}}>Заходи,
                        регистрируйся, и побеждай
                    </div>
                </Box>
                <Box className={classes.mainWrapper}>
                    <TabsUnstyled defaultValue={0}>
                        <TabsList>
                            <Tab>В 1 клик</Tab>
                            <Tab>По E-mail</Tab>
                            <Tab>Соц.сети</Tab>
                        </TabsList>
                        <TabPanel value={0}>
                            <ReactFlagsSelect
                                className={classes.countrySelector}
                                selected={select}
                                onSelect={onSelect}
                                countries={["RU", "GB", "IE", "IT", "NL", "SE", 'KZ']}
                            />
                            <Box className={classes.fieldWrapper}>
                                <Box className={classes.fieldWrapper2}>
                                    <span style={{fontWeight: '700', fontSize: '15px', marginLeft: '15px'}}>Добавить промокод</span>
                                    <button className={classes.smallButton}>+</button>
                                </Box>
                            </Box>
                            <Divider orientation='horizontal'
                                     style={{
                                         width: '421px',
                                         background: '#FFFFFF',
                                         marginTop: '30px',
                                         marginLeft: '-39px'
                                     }}/>
                            <Box style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                padding: '10px',
                                alignItems: 'center'
                            }}>
                                <Checkbox defaultChecked/>
                                <span style={{
                                    fontSize: '10px',
                                    fontWeight: 400,
                                    textAlign: 'left',
                                    color: 'white',
                                    lineHeight: '11px'
                                }}>Я Подтверждаю, что я ознакомлен и полностью согласен с Условиями Соглашения об использовании сайта</span>
                            </Box>
                            <Box>
                                <button className={classes.loginButton}
                                        onClick={() => handleOpen('one')}>Зарегистрироваться
                                </button>
                            </Box>
                        </TabPanel>
                        <TabPanel value={1}>
                            <ReactFlagsSelect
                                className={classes.countrySelector}
                                selected={select}
                                onSelect={onSelect}
                                countries={["RU", "GB", "IE", "IT", "NL", "SE", 'KZ']}
                            />
                            <Box className={classes.fieldWrapper}>
                                <Box className={classes.fieldWrapper2}>
                                    <span style={{fontWeight: '700', fontSize: '15px', marginLeft: '15px'}}>Добавить промокод</span>
                                    <button className={classes.smallButton}>+</button>
                                </Box>
                            </Box>
                            <Divider orientation='horizontal'
                                     style={{width: '425px', background: '#FFFFFF', marginTop: '30px'}}/>
                            <Box style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                padding: '10px 30px',
                                alignItems: 'center'
                            }}>
                                <Checkbox defaultChecked/>
                                <span style={{
                                    fontSize: '10px',
                                    fontWeight: 400,
                                    textAlign: 'left',
                                    color: 'white',
                                    lineHeight: '11px'
                                }}>Я Подтверждаю, что я ознакомлен и полностью согласен с Условиями Соглашения об использовании сайта</span>
                            </Box>
                            <Box>
                                <button className={classes.loginButton}>Зарегистрироваться</button>
                            </Box>
                        </TabPanel>
                        <TabPanel value={2}>
                            <ReactFlagsSelect
                                className={classes.countrySelector}
                                selected={select}
                                onSelect={onSelect}
                                countries={["RU", "GB", "IE", "IT", "NL", "SE", 'KZ']}
                            />
                            <Box className={classes.fieldWrapper}>
                                <Box className={classes.fieldWrapper2}>
                                    <span style={{fontWeight: '700', fontSize: '15px', marginLeft: '15px'}}>Добавить промокод</span>
                                    <button className={classes.smallButton}>+</button>
                                </Box>
                            </Box>
                            <Divider orientation='horizontal'
                                     style={{width: '425px', background: '#FFFFFF', marginTop: '30px'}}/>
                            <Box style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                padding: '10px 30px',
                                alignItems: 'center'
                            }}>
                                <Checkbox defaultChecked/>
                                <span style={{
                                    fontSize: '10px',
                                    fontWeight: 400,
                                    textAlign: 'left',
                                    color: 'white',
                                    lineHeight: '11px'
                                }}>Я Подтверждаю, что я ознакомлен и полностью согласен с Условиями Соглашения об использовании сайта</span>
                            </Box>
                            <Box>
                                <button className={classes.loginButton}>Зарегистрироваться</button>
                            </Box>
                        </TabPanel>
                    </TabsUnstyled>
                </Box>
            </Box>
            <Box style={{marginTop: '10px'}}>
                <span style={{
                    fontSize: '13px',
                    fontWeight: 400,
                    textAlign: 'left',
                    color: 'white',
                    opacity: 0.2,
                    lineHeight: '11px',
                    marginRight: '5px'
                }}>У меня уже есть аккаунт {' '} </span>
                <span className={classes.lowerLink}>{'  '} Войти</span>
            </Box>
            <Box className={classes.leftWing}/>
            <Box className={classes.rightWing}>
                <Stack direction="column" spacing={5}
                       style={{transform: 'rotate(-90deg) translateY(115px) translateX(45px)', width: '180px'}}>
                    <img src={ball} alt='alt'/>
                    <Divider orientation="horizontal" style={{width: '190px', background: 'white', opacity: 0.1}}/>
                    <img src={cubes} alt='alt'/>
                    <Divider orientation="horizontal" style={{width: '190px', background: 'white', opacity: 0.1}}/>
                    <img src={tvspot} alt='alt'/>
                </Stack>
            </Box>
            <OneClickModal open={openOnClick} handleClose={() => handleClose('one')}/>
        </Box>
    );
}

export default Registration;