import {makeStyles} from "@mui/styles";


export const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        left: '164px',
        top: '144px',
        width: '425px',
        height: '571px',
        boxShadow: '23px 0px 24px rgba(0, 0, 0, 0.2)',
        backdropFilter: 'blur(27.1828px)',
        borderRadius: '20px',
        background: 'radial-gradient(116.55% 100.44% at 1.18% 0.44%, rgba(148, 166, 205, 0.2) 0%, rgba(148, 166, 205, 2e-05) 100%)'
    },
    title: {
        fontWeight: 800,
        fontSize: '41px',
        lineHeight: '46px',
        color: '#FFF'
    },
    mainWrapper: {
        display: 'flex',
        flexDirection: 'column',
        padding: '10px 15px 0 20px'

    },
    tabsWrapper: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '30px'
    },
    fieldWrapper: {
        background: 'rgba(148, 166, 205, 0.1)',
        width: '345px',
        height: '49.2px',
        borderRadius: '10px',
        marginTop: '10px',
        color: 'white',
        position: 'relative',
    },
    fieldWrapper2: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px 10px'
    },
    smallButton: {
        background: 'linear-gradient(132.58deg, #A5E313 15.04%, #80C100 86.17%)',
        borderRadius: '12.5px',
        fontSize: '24px',
        color: 'white',
        border: 'none',
        width: '28px',
        height: '28px',
    },
    loginButton: {
        background: 'linear-gradient(91.16deg, #A5E313 0%, #80C100 100%)',
        boxShadow: '0px 8px 62px rgba(122, 195, 8, 0.59)',
        borderRadius: '10px',
        width: '345px',
        height: '49px',
        border: 'none',
        color: '#FFF',
        fontSize: '15px',
        fontWeight: 700
    },
    countrySelector: {
        background: '#94A6CD1A',
        borderRadius: '10px',
        height: '49px',
        '& .ReactFlagsSelect-module_selectBtn__19wW7': {
            border: 'none',
            color: '#FFF',
            padding: '11px 20px'
        }
    },
    lowerLink: {
        fontSize: '15px',
        fontWeight: 700,
        textAlign: 'left',
        color: 'white',
        lineHeight: '11px',
        '&:hover': {
            color: '#80C100',
        }
    },
    leftWing: {
        background: 'radial-gradient(184.25% 184.25% at 0% 0%, rgba(148, 166, 205, 0.1) 0%, rgba(148, 166, 205, 1e-05) 100%)',
        borderTop: '25px solid #94A6CD1A',
        borderLeft: '25px solid transparent',
        borderRight: '25px solid transparent',
        height: 0,
        width: '420px',
        transform: 'translateY(-259px) translateX(-247px) rotate(90deg)'
    },
    rightWing: {
        background: 'radial-gradient(184.25% 184.25% at 0% 0%, rgba(148, 166, 205, 0.1) 0%, rgba(148, 166, 205, 1e-05) 100%)',
        borderBottom: '250px solid #94A6CD1A',
        borderLeft: '25px solid transparent',
        borderRight: '25px solid transparent',
        height: 0,
        width: '415px',
        transform: 'translateY(-393px) translateX(317px) rotate(90deg)',
        position: 'relative'
    },
}));