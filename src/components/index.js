import React from 'react';
import Payments from "./payments";
import Registration from "./registrationModal";

function Index() {
    return (
        <>
            <Registration/>
            <Payments/>
        </>
    );
}

export default Index;